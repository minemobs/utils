package fr.minemobs.knockback.Events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import fr.minemobs.knockback.Main;

public class onDamage implements Listener {

    Main plugin;

    public onDamage(Main plugin) {
        this.plugin = plugin;
    }  
   
    @EventHandler
    public void onDamageEvent(EntityDamageEvent e){
       
        if(e.getEntity() instanceof Player){
           
            Entity p = e.getEntity();
           
            p.setVelocity(p.getLocation().getDirection().multiply(2));
           
        }
       
    }

}
 