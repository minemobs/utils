package fr.minemobs.knockback.Events;

import java.util.Arrays;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import fr.minemobs.knockback.Main;

import sun.java2d.loops.DrawLine;

public class EventManager implements Listener {
	private Main plugin;
	
	public EventManager(Main plugin) {
		this.plugin = plugin;
	}

	
	@EventHandler
	public void grappleEvent(EntityShootBowEvent event) {
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			Arrow arrow = (Arrow) event.getProjectile();
			//arrow.setVelocity(arrow.getVelocity().multiply(2D));
			
			ItemStack tpStick = new ItemStack(Material.BOW, 1);
			ItemMeta tpStickM = tpStick.getItemMeta();
			
			tpStickM.setLore(Arrays.asList("It's littrly a grappling hook"));
			
			tpStickM.setDisplayName("Grappin");
			
			tpStickM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			tpStickM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			tpStick.setItemMeta(tpStickM);

			new BukkitRunnable() {
				
				@Override
				public void run() {
					if(arrow.getLocation().distance(player.getLocation()) > 50) {
						arrow.remove();
						this.cancel();
					}
					
					if(player.getInventory().getItemInHand().getType().equals(Material.BOW) && player.getInventory().getItemInHand().getItemMeta().equals(tpStickM)) {
					
					if(player.isSneaking()) {
						player.setVelocity(player.getLocation().getDirection().multiply(2D));
						arrow.remove();
						this.cancel();
					}
					
					if(arrow.isOnGround() && !arrow.isDead()) {
						Vector direction = arrow.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
						player.setVelocity(direction.multiply(1.2D));
						DrawLine(player.getLocation(), arrow.getLocation(),2);
						if(player.getLocation().distance(arrow.getLocation()) <= 3) {
							this.cancel();
							arrow.remove();
						}
					}
					}else if (!player.getInventory().getItemInHand().getType().equals(Material.BOW) || !player.getInventory().getItemInHand().getItemMeta().equals(tpStickM)) {
						this.cancel();
					}
				}		
			}.runTaskTimer(plugin, 0, 0);
		}
	}
	
	private void DrawLine(Location player, Location arrow, double space) {
		World world = player.getWorld();
		Validate.isTrue(arrow.getWorld().equals(world), "Les Particles ne peuvent pas �tre dessin�es dans diff�rents mondes");
		
		double distance = player.distance(arrow);
		
		Vector p1 = player.toVector();
		Vector p2 = arrow.toVector();
		
		Vector vector = p2.clone().subtract(p1).normalize().multiply(space);
		
		double covered = 0;
		
		for(;covered < distance; p1.add(vector)) {
            covered += space;
		}
		
	}
	
	
}
