package fr.minemobs.knockback.Events;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class onDraconicEquip implements Listener {
	
	@EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        Player p = (Player) e.getPlayer();
        if (p.getInventory().getBoots() != null && p.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase("Draconic boots")) {
            PotionEffect speed = new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2);
            p.addPotionEffect(speed);
            p.setAllowFlight(true);
        } else {
            if (p.hasPotionEffect(PotionEffectType.SPEED)) {
                p.removePotionEffect(PotionEffectType.SPEED);
                if(p.getGameMode() == GameMode.CREATIVE || p.getGameMode() == GameMode.SPECTATOR) {
                	return;
                }
                
                p.setAllowFlight(false);
                
            }
        }
     
     
     
    }	
	
}
	
