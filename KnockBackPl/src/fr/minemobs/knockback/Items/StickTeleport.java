package fr.minemobs.knockback.Items;

import java.util.Arrays;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class StickTeleport implements Listener, CommandExecutor {
	
	  @EventHandler
	  public void onPlayerInteract(PlayerInteractEvent e) {
	    if (e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
	      Player p = e.getPlayer();
	      
			ItemStack tpStick = new ItemStack(Material.STICK, 1);
			ItemMeta tpStickM = tpStick.getItemMeta();
			
			tpStickM.setLore(Arrays.asList("Vous t�l�port � l'endoit que vous regardez"));
			
			tpStickM.setDisplayName("Tp Stick");
			
			tpStickM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			tpStickM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			tpStick.setItemMeta(tpStickM);
	      
	      if (p.getInventory().getItemInHand().getType().equals(Material.STICK) && p.getInventory().getItemInHand().getItemMeta().equals(tpStickM)) {
	        Block b = p.getTargetBlock((Set)null, 100);
	        p.teleport(b.getLocation().add(0.0D, 1.0D, 0.0D));
	      } 
	    } 

}
	  
		@Override
		public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
			if(sender instanceof Player) {
				Player player = (Player) sender;
			
			ItemStack tpStick = new ItemStack(Material.STICK, 1);
			ItemMeta tpStickM = tpStick.getItemMeta();
			
			tpStickM.setLore(Arrays.asList("Vous t�l�port � l'endoit que vous regardez"));
			
			tpStickM.setDisplayName("Tp Stick");
			
			tpStickM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			tpStickM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			tpStick.setItemMeta(tpStickM);

			
			player.getInventory().addItem(tpStick);

		}
			return true;
	}
	
}
