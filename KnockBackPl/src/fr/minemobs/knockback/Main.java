package fr.minemobs.knockback;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minemobs.knockback.Events.EventManager;
import fr.minemobs.knockback.Events.OnInteract;
import fr.minemobs.knockback.Events.onDraconicEquip;
import fr.minemobs.knockback.Items.StickTeleport;
import fr.minemobs.knockback.commands.BackpackCommand;
import fr.minemobs.knockback.commands.CommandAllowFly;
import fr.minemobs.knockback.commands.CommandColor;
import fr.minemobs.knockback.commands.CommandCraft;
import fr.minemobs.knockback.commands.CommandFakeReload;
import fr.minemobs.knockback.commands.CommandFurnace;
import fr.minemobs.knockback.commands.CommandHeal;
import fr.minemobs.knockback.commands.CommandNick;
import fr.minemobs.knockback.commands.CommandSpawnPoint;
import fr.minemobs.knockback.commands.EnchantGive;
import fr.minemobs.knockback.commands.EnderChestCommand;
import fr.minemobs.knockback.commands.PingCommand;
import fr.minemobs.knockback.enchants.CustomEnchantment;
import fr.minemobs.knockback.enchants.FlyStick;
import fr.minemobs.knockback.enchants.LightBolt;
import fr.minemobs.knockback.enchants.Pickaxe;
import fr.minemobs.knockback.enchants.TeamTree;
import fr.minemobs.knockback.gen.CustomChunkGenerator;
import fr.minemobs.knockback.listeners.PlayerCloseInventoryListener;

public class Main extends JavaPlugin implements Listener{
	   
public static Main instance;

public CustomEnchantment ench = new CustomEnchantment(101);
public Pickaxe ench1 = new Pickaxe(103);
public LightBolt ench2 = new LightBolt(102);
public FlyStick ench3 = new FlyStick(104);
public TeamTree ench4 = new TeamTree(105);

public String ebheader = ChatColor.translateAlternateColorCodes('&', "&8[&4Minemobs Utils&8]");

    public static Main getInstance(){
       
        return instance;
               
    }
   
    @Override
    public void onEnable(){
       
        super.onEnable();
        
        instance = this;
        
        try {
            try {
              Field f = Enchantment.class.getDeclaredField("acceptingNew");
              f.setAccessible(true);
              f.set((Object)null, Boolean.valueOf(true));
            } catch (Exception e) {
              e.printStackTrace();
            } 
            try {
              Enchantment.registerEnchantment((Enchantment)this.ench);
              Enchantment.registerEnchantment((Enchantment)this.ench1);
              Enchantment.registerEnchantment((Enchantment)this.ench2);
              Enchantment.registerEnchantment((Enchantment)this.ench3);
              Enchantment.registerEnchantment((Enchantment)this.ench4);
            } catch (IllegalArgumentException e) {
              e.printStackTrace();
            } 
          } catch (Exception e) {
            e.printStackTrace();
          }
        
        Bukkit.getConsoleSender().sendMessage(this.ebheader + ChatColor.GREEN + " Enabled.");
        
        new PlayerCloseInventoryListener(this);
        new BackpackCommand(this);
        
        this.getCommand("afly").setExecutor(new CommandAllowFly());
        this.getCommand("heal").setExecutor(new CommandHeal());
        this.getCommand("tpsp").setExecutor(new CommandSpawnPoint());
        this.getCommand("nick").setExecutor(new CommandNick());
        this.getCommand("cc").setExecutor(new CommandColor());
        this.getCommand("ping").setExecutor(new PingCommand());
        this.getCommand("frl").setExecutor(new CommandFakeReload());
        this.getCommand("tpsg").setExecutor(new StickTeleport());
        this.getCommand("enchants").setExecutor(new EnchantGive());
        this.getCommand("craft").setExecutor(new CommandCraft());
        this.getCommand("enderchest").setExecutor(new EnderChestCommand());
       
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new OnInteract(), this);
        pm.registerEvents(new StickTeleport(), this);
        pm.registerEvents(new onDraconicEquip(), this);
        pm.registerEvents(new EventManager(this), this);

        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(this.ench1, this);
        getServer().getPluginManager().registerEvents(this.ench2, this);
        getServer().getPluginManager().registerEvents(this.ench, this);
        getServer().getPluginManager().registerEvents(this.ench3, this);
        getServer().getPluginManager().registerEvents(this.ench4, this);
        
		      
    }
    
    //gen
    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new CustomChunkGenerator();
    }
       
    @SuppressWarnings("unlikely-arg-type")
	public void onDisable() {
		try {
			Field byIdField = Enchantment.class.getDeclaredField("byId");
			Field byNameField = Enchantment.class.getDeclaredField("byName");

			byIdField.setAccessible(true);
			byNameField.setAccessible(true);

			HashMap<Integer, Enchantment> byId = (HashMap<Integer, Enchantment>) byIdField.get(null);
			HashMap<Integer, Enchantment> byName = (HashMap<Integer, Enchantment>) byNameField.get(null);

			if (byId.containsKey(ench.getId())) {
				byId.remove(ench.getId());
			}else if(byId.containsKey(ench1.getId())) {
				byId.remove(ench1.getId());
			}else if(byId.containsKey(ench2.getId())) {
				byId.remove(ench2.getId());
			}

			if (byName.containsKey(ench.getName())) {
				byName.remove(ench.getName());
			} else if (byName.containsKey(ench1.getName())) {
				byName.remove(ench1.getName());
			}else if (byName.containsKey(ench2.getName())) {
				byName.remove(ench2.getName());
			}
		} catch (Exception ignored) {
		}
	}

    /*/@EventHandler
    public void onDamageEvent(EntityDamageByEntityEvent e){

        if (e.getDamager() instanceof Player) {
            Player damager = (Player) e.getDamager();

            Entity p = e.getEntity();

            p.setVelocity(new Vector(p.getVelocity().getX() -2, 1.0D, p.getVelocity().getZ() -2));
        }
                
    }/*/
    
    //tpStick
   
}
 