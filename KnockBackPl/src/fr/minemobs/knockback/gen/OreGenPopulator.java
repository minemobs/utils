package fr.minemobs.knockback.gen;

import java.util.Random;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

public class OreGenPopulator extends BlockPopulator{

		@Override
		public void populate(World world, Random random, Chunk chunk) {
			int X, Y, Z;
			boolean isStone;
			for (int i = 1; i < 15; i++) {  // Nombre d'essais
			    if (random.nextInt(100) < 60) {  // Taux de chance de spawn
				X = random.nextInt(15);
				Z = random.nextInt(15);
				Y = random.nextInt(40)+20;  // Get randomized coordinates
				if (chunk.getBlock(X, Y, Z).getType() == Material.STONE) {
					isStone = true;
					while (isStone) {
						chunk.getBlock(X, Y, Z).setType(Material.COAL_ORE);
						if (random.nextInt(100) < 40)  {   // Taux de change pour continuer le fillon.
							switch (random.nextInt(5)) {  // La direction choisie
							case 0: X++; break;
							case 1: Y++; break;
							case 2: Z++; break;
							case 3: X--; break;
							case 4: Y--; break;
							case 5: Z--; break;
							}
							isStone = (chunk.getBlock(X, Y, Z).getType() == Material.STONE) && (chunk.getBlock(X, Y, Z).getType() != Material.COAL_ORE);
						} else isStone = false;
					}
				}
			    }
			}
		}
}
