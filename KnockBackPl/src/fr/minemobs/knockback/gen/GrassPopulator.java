package fr.minemobs.knockback.gen;

import java.util.Random;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

public class GrassPopulator extends BlockPopulator {

	@Override
	public void populate(World world, Random random, Chunk chunk) {
		if (random.nextBoolean()) {
			int amout = random.nextInt(4)+1;
			for (int i  = 1; i < amout; i++) {
				int X = random.nextInt(15);
				int Y;
				int Z = random.nextInt(15);
				for (Y = world.getMaxHeight()-1; chunk.getBlock(X, Y, Z).getType() == Material.AIR; Y--);
				
				chunk.getBlock(X, Y+1, Z).setType(Material.GRASS);
			}
		}
		
	}
}
