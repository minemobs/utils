package fr.minemobs.knockback.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import fr.minemobs.knockback.Main;

public class PlayerCloseInventoryListener implements Listener {
  private Main plugin;
  
  public PlayerCloseInventoryListener(Main plugin) {
    this.plugin = plugin;
    Bukkit.getPluginManager().registerEvents(this, (Plugin)plugin);
  }
  
  @EventHandler
  public void onInvClose(InventoryCloseEvent e) {
    String title = e.getInventory().getTitle();
    if (!title.equalsIgnoreCase(ChatColor.GREEN + "Backpack"))
      return; 
    Inventory inv = e.getInventory();
    this.plugin.getConfig().set(e.getPlayer().getUniqueId().toString(), null);
    for (int i = 0; i <= 35; i++) {
      ItemStack item = inv.getItem(i);
      if (item != null && 
        item.getType() != Material.AIR)
        this.plugin.getConfig().set(e.getPlayer().getUniqueId().toString() + "." + i, item); 
    } 
    this.plugin.saveConfig();
  }
}
