package fr.minemobs.knockback.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpawnPoint implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			player.teleport(player.getBedSpawnLocation());
			player.sendTitle("T�l�portation finie", "Vous avez �t� tp � votre spawn point");
			}
			
		return true;
	}

}
