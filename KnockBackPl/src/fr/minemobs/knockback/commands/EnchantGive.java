package fr.minemobs.knockback.commands;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.minemobs.knockback.Main;
import fr.minemobs.knockback.enchants.CustomEnchantment;
import fr.minemobs.knockback.enchants.FlyStick;
import fr.minemobs.knockback.enchants.LightBolt;
import fr.minemobs.knockback.enchants.Pickaxe;
import fr.minemobs.knockback.enchants.TeamTree;

public class EnchantGive implements CommandExecutor {


	public Main instance;
	public CustomEnchantment ench = new CustomEnchantment(101);
	public LightBolt ench1 = new LightBolt(102);
	public Pickaxe ench2 = new Pickaxe(103);
	public FlyStick ench3 = new FlyStick(104);
	public TeamTree ench4 = new TeamTree(105);
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;

			ItemStack item = new ItemStack(Material.DIAMOND_AXE);
			ItemMeta meta = item.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + ench.getName() + " I");
			meta.setDisplayName(ChatColor.GOLD + "Explosive Axe");
			meta.setLore(lore);
			item.setItemMeta(meta);
			item.addUnsafeEnchantment(ench, 1);
			
			ItemStack TStick = new ItemStack(Material.STICK);
			ItemMeta TMeta = TStick.getItemMeta();
			ArrayList<String> Tlore = new ArrayList<String>();
			Tlore.add(ChatColor.GRAY + ench4.getName() + " I");
			TMeta.setDisplayName(ChatColor.GOLD + "B�ton de Team Tree");
			TMeta.setLore(Tlore);
			TStick.setItemMeta(TMeta);
			TStick.addUnsafeEnchantment(ench4, 1);
			
			ItemStack p = new ItemStack(Material.DIAMOND_PICKAXE);
			ItemMeta pMeta = p.getItemMeta();
			ArrayList<String> plore = new ArrayList<String>();
			plore.add(ChatColor.GRAY + ench2.getName() + " I");
			pMeta.setDisplayName(ChatColor.GOLD + "El famosso Hammer");
			pMeta.setLore(plore);
			p.setItemMeta(pMeta);
			p.addUnsafeEnchantment(ench2, 1);
			
			ItemStack fs = new ItemStack(Material.STICK);
			ItemMeta fsMeta = p.getItemMeta();
			ArrayList<String> fslore = new ArrayList<String>();
			fslore.add(ChatColor.GRAY + ench3.getName() + " I");
			fsMeta.setDisplayName(ChatColor.GOLD + "It's not rlly a kb stick");
			fsMeta.setLore(fslore);
			fs.setItemMeta(fsMeta);
			fs.addUnsafeEnchantment(ench3, 1);
			
			ItemStack gBow = new ItemStack(Material.BOW, 1);
			ItemMeta gBowM = gBow.getItemMeta();
			
			gBowM.setLore(Arrays.asList("It's littrly a grappling hook"));
			
			gBowM.setDisplayName("Grappin");
			
			gBowM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			gBowM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			gBow.setItemMeta(gBowM);
			
			ItemStack zeusStick = new ItemStack(Material.STICK);
			ItemMeta zsMeta = zeusStick.getItemMeta();
			ArrayList<String> zslore = new ArrayList<String>();
			zslore.add(ChatColor.GRAY + ench1.getName() + " I");
			zsMeta.setDisplayName(ChatColor.GOLD + "B�ton de Zeus");
			zsMeta.setLore(zslore);
			zeusStick.setItemMeta(zsMeta);
			zeusStick.addUnsafeEnchantment(ench1, 1);
						
			player.getInventory().addItem(item);
						
			player.getInventory().addItem(zeusStick);
			
			player.getInventory().addItem(p);
			
			player.getInventory().addItem(fs);
			
			player.getInventory().addItem(gBow);
			
			player.getInventory().addItem(TStick);
			
		}
			
		return true;
	}

}
