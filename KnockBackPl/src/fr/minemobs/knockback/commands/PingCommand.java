package fr.minemobs.knockback.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.EntityPlayer;

public class PingCommand implements CommandExecutor {
	
	@Override
 public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Player p = (Player)sender;
        if (args.length == 0) {
            if (p instanceof Player) {
              p.sendMessage("�7[�aLag�7] �4Pong �eVous Avez: �f" + getPing(p) + " �ems");
            } else {
              p.sendMessage("�7[�aLag�7] �eCe joueur n'est pas en ligne!");
            } 
          } else if (args.length == 1) {
            Player target = Bukkit.getPlayer(args[0]);
            if (target instanceof Player) {
              p.sendMessage("�7[�cLag�7] �eLe Joueur�f" + target.getName() + " �e�: �f " + getPing(target) + " �ems");
            } else {
              p.sendMessage("�7[�aLag�7] �eCe joueur n'as jamais rejoint le serveur!");
            } 
          
    }
    return true;
	}

	
	  public int getPing(Player p) {
		    CraftPlayer pingc = (CraftPlayer)p;
		    EntityPlayer pinge = pingc.getHandle();
		    return pinge.ping;
		  }

}
