package fr.minemobs.knockback.commands;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandColor implements CommandExecutor {

	String ajouts_des_blocks = "";
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			Inventory colorcode = Bukkit.getServer().createInventory(player, 27, "Codes couleur");
			
			//laine rouge
			ItemStack red = new ItemStack(Material.WOOL, 1, (byte)14);
			ItemMeta redM = red.getItemMeta();
			
			redM.setLore(Arrays.asList("&4"));
			
			redM.setDisplayName("Rouge");
			
			redM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			redM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			red.setItemMeta(redM);
			//laine verte
			ItemStack green = new ItemStack(Material.WOOL, 1, (byte)5);
			ItemMeta greenM = green.getItemMeta();
			
			greenM.setLore(Arrays.asList("&a"));
			
			greenM.setDisplayName("Vert");
			
			greenM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			greenM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			green.setItemMeta(greenM);
			//laine noir
			ItemStack black = new ItemStack(Material.WOOL, 1, (byte)15);
			ItemMeta blackM = black.getItemMeta();
			
			blackM.setLore(Arrays.asList("&0"));
			
			blackM.setDisplayName("Noir");
			
			blackM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			blackM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			black.setItemMeta(blackM);
			//laine blanche
			ItemStack white = new ItemStack(Material.WOOL, 1, (byte)0);
			ItemMeta whiteM = white.getItemMeta();
			
			whiteM.setDisplayName("Blanc");
			
			whiteM.setLore(Arrays.asList("&f"));
			
			whiteM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			whiteM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			white.setItemMeta(whiteM);
			//laine jaune
			ItemStack jaune = new ItemStack(Material.WOOL, 1, (byte)4);
			ItemMeta jauneM = jaune.getItemMeta();
			
			jauneM.setDisplayName("Jaune");
			
			jauneM.setLore(Arrays.asList("&e"));
			
			jauneM.addEnchant(Enchantment.DURABILITY, 200, true);
			
			jauneM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			
			jaune.setItemMeta(jauneM);
			
			ajouts_des_blocks = "";
			
			colorcode.setItem(0, red);
			
			colorcode.setItem(2, green);
			
			colorcode.setItem(6, black);
			
			colorcode.setItem(8, white);
			
			colorcode.setItem(4, jaune);
			
			//ouvre le gui
			player.openInventory(colorcode);
			
			
		}
		return true;
	}

}
