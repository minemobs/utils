package fr.minemobs.knockback.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFakeReload implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			Bukkit.getServer().broadcastMessage("�cPlease note that this command is not supported and may cause issues when using some plugins."); 
			Bukkit.getServer().broadcastMessage(ChatColor.RED + "If you encounter any issues please use the /stop command to restart your server.");
			Bukkit.getServer().broadcastMessage("�aReload complete.");
	}
		return true;
	}
}
