package fr.minemobs.knockback.enchants;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Pickaxe extends Enchantment implements Listener {

	public Pickaxe(int id) {
		super(id);
	}
	
	Map<UUID, BlockFace> lastBlockFace = new HashMap<>();
	  
	  @EventHandler(priority = EventPriority.HIGHEST)
	  public void onBlockBreak(BlockBreakEvent e) {
	    ItemStack i;
	    Player p = e.getPlayer();
	    try {
	      if (e.getPlayer().getInventory().getItemInHand() == null || e.getPlayer().getInventory().getItemInHand().getType() == Material.AIR)
	        return; 
	      i = e.getPlayer().getInventory().getItemInHand();
	    } catch (NoSuchMethodError ex) {
	      if (e.getPlayer().getItemInHand() == null || e.getPlayer().getItemInHand().getType() == Material.AIR)
	        return; 
	      i = e.getPlayer().getItemInHand();
	    } 
	    if (i.getItemMeta().getLore() == null || i.getItemMeta().getLore().isEmpty() || !i.containsEnchantment(this))
	      return; 
	    if (i.getType() != Material.WOOD_PICKAXE && i.getType() != Material.STONE_PICKAXE && i.getType() != Material.IRON_PICKAXE && i.getType() != Material.GOLD_PICKAXE && i.getType() != Material.DIAMOND_PICKAXE)
	      return; 
	    if (!p.hasPermission("minemobsutils.hammer.use")) {
	      e.setCancelled(true);
	      e.setExpToDrop(0);
	      return;
	    }
	    if (this.lastBlockFace.get(p.getUniqueId()) == null || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.DOWN || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.UP) {
	      processBlock(i, e.getBlock().getRelative(BlockFace.NORTH), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.NORTH_EAST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.EAST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH_EAST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH_WEST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.WEST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.NORTH_WEST), p);
	    } 
	    if (this.lastBlockFace.get(p.getUniqueId()) == BlockFace.EAST || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.WEST) {
	      processBlock(i, e.getBlock().getRelative(BlockFace.UP), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.DOWN), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.NORTH), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.NORTH).getRelative(BlockFace.UP), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.NORTH).getRelative(BlockFace.DOWN), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH).getRelative(BlockFace.DOWN), p);
	    } 
	    if (this.lastBlockFace.get(p.getUniqueId()) == BlockFace.NORTH || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.SOUTH) {
	      processBlock(i, e.getBlock().getRelative(BlockFace.UP), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.DOWN), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.EAST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.WEST), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.EAST).getRelative(BlockFace.UP), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.EAST).getRelative(BlockFace.DOWN), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.WEST).getRelative(BlockFace.UP), p);
	      processBlock(i, e.getBlock().getRelative(BlockFace.WEST).getRelative(BlockFace.DOWN), p);
	    } 
	  }
	  
	  @EventHandler(priority = EventPriority.HIGHEST)
	  public void onPlayerInteract(PlayerInteractEvent e) {
	    if (e.getAction() == Action.LEFT_CLICK_BLOCK)
	      this.lastBlockFace.put(e.getPlayer().getUniqueId(), e.getBlockFace()); 
	  }
	  
	  private void processBlock(ItemStack i, Block b, Player p) {
	    if (b.isLiquid())
	      return; 
	    if (b.getType() == Material.BEDROCK || b.getType() == Material.BARRIER || b.getType() == Material.MOB_SPAWNER || b.getType() == Material.ICE || b.getType() == Material.PACKED_ICE || b.getType() == Material.ENDER_CHEST)
	      return; 
	    boolean shouldBreak = true;
	    switch (i.getType()) {
	      case WOOD_PICKAXE:
		        if (b.getType() != Material.STONE)
	          shouldBreak = false; 
	        break;
	      case STONE_PICKAXE:
		        if (b.getType() != Material.STONE)
	          shouldBreak = false; 
	        break;
	      case IRON_PICKAXE:
	        if (b.getType() != Material.STONE)
	          shouldBreak = false; 
	        break;
	      case GOLD_PICKAXE:
	        if (b.getType() != Material.STONE)
	          shouldBreak = false; 
	      case DIAMOND_PICKAXE:
		        if (b.getType() != Material.STONE)
		          shouldBreak = false;
	        break;
		default:
			shouldBreak = false;
			break;
	    } 
	    if (shouldBreak)
	      b.breakNaturally(i); 
	  }
	
	@Override
	public int getId() {
		return 103;
	}

	@Override
	public boolean canEnchantItem(ItemStack arg0) {
		return true;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}
	
	@Override
	public int getMaxLevel() {
		return 3;
	}

	@Override
	public String getName() {
		return "Random";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}


}
