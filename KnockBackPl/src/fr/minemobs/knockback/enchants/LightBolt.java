package fr.minemobs.knockback.enchants;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LightBolt extends Enchantment implements Listener {

	public LightBolt(int id) {
		super(id);

	}

	@EventHandler
	public void onHit(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player){
			Player player = (Player) event.getDamager();

			if(player.getItemInHand().containsEnchantment(this)) {
				player.getWorld().strikeLightning(event.getEntity().getLocation());
			}
		}
	}

	@Override
	public int getId() {
		return 102;
	}

	@Override
	public boolean canEnchantItem(ItemStack arg0) {
		return true;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}
	
	@Override
	public int getMaxLevel() {
		return 3;
	}

	@Override
	public String getName() {
		return "Zeus";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}


}
